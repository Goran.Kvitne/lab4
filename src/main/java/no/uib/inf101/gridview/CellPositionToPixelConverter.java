package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  // TODO: Implement this class

  private Rectangle2D box;
  private double margin;
  private double cellWidth;
  private double cellHeight;
  
  
  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){

    this.box = box;
    this.margin = margin;

    cellWidth = (box.getWidth() - margin * (gd.cols() +1)) / gd.cols();
    cellHeight = (box.getHeight() - margin * (gd.rows() + 1)) /gd.rows();

  }

  public Rectangle2D getBoundsForCell(CellPosition cp){
    
    double x = box.getX() + margin * (cp.col() + 1) + cellWidth * cp.col();
    double y = box.getY() + margin * (cp.row() + 1) + cellHeight * cp.row();

    return new Rectangle2D.Double(x, y, cellWidth, cellHeight); 
    
    
  }
}
