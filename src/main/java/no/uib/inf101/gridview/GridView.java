package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {
  // TODO: Implement this class

  IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid) {
    this.colorGrid = colorGrid;
    this.setPreferredSize(new Dimension(400, 300));
  }


  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    drawGrid();
    drawCells();
    getBoundsForCell();

  }

  private void getBoundsForCell() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'getBoundsForCell'");
  }


  private void drawCells() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'drawCells'");
  }


  public void drawGrid(Graphics2D g) {
    
    Rectangle2D gridbox = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN,  
      this.getWidth() - 2 * OUTERMARGIN, this.getHeight() - 2 * OUTERMARGIN);
    
    
    g.setColor(MARGINCOLOR);
    g.fill(gridbox);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter()

   






}
}