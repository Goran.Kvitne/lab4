package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{


  private int rows;
  private int cols;

  Color[][] grid;


  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;

    grid = new Color[rows][cols];
    for(int row = 0; row < rows; row++) {
      for(int col = 0; col < cols; col++){
        grid[row][col] = null;
      }
    }

  }

  @Override
  public int rows() {
    // TODO Auto-generated method stub
    return rows;
  }

  @Override
  public int cols() {
    // TODO Auto-generated method stub
    return cols;
  }

  @Override
  public List<CellColor> getCells() {
    // TODO Auto-generated method stub
    List<CellColor> result = new ArrayList<>();
    for(int row = 0; row < rows; row++) {
      for(int col = 0; col < cols; col++){
        CellPosition pos = new CellPosition(row, col);
        result.add(new CellColor(pos, get(pos)));
      }
    }
    return result;
  }

  @Override
  public Color get(CellPosition pos) {
    
    return grid[pos.row()][pos.col()]; 
  }

  @Override
  public void set(CellPosition pos, Color color) {
    // TODO Auto-generated method stub
    grid[pos.row()][pos.col()] = color;
  }
  // TODO: Implement this class

 

}
